from abc import ABC, abstractmethod

class Transport(ABC):
    @abstractmethod
    def move(self):
        pass

class Bus(Transport):
    def move(self):
        print("Bus is moving")

class Subway(Transport):
    def move(self):
        print("Subway is moving")

class Taxi(Transport):
    def move(self):
        print("Taxi is moving")

class TransportFactory:
    @staticmethod
    def create_transport(transport_type):
        if transport_type == "bus":
            return Bus()
        elif transport_type == "subway":
            return Subway()
        elif transport_type == "taxi":
            return Taxi()
        else:
            raise ValueError("Unknown transport type")
        
class Observable:
    def __init__(self):
        self.observers = []

    def add_observer(self, observer):
        self.observers.append(observer)

    def remove_observer(self, observer):
        self.observers.remove(observer)

    def notify_observers(self, data):
        for observer in self.observers:
            observer.update(data)

class TrafficAnalyzer(Observable):
    def analyze_traffic(self):
        # Анализ трафика
        data = "Traffic data"
        self.notify_observers(data)

class Alert:
    def update(self, data):
        print("Alert! New data: " + str(data))

class Logger:
    def update(self, data):
        print("Logging new data: " + str(data))

class TrafficAnalyzerDecorator(TrafficAnalyzer):
    def __init__(self, traffic_analyzer):
        self.traffic_analyzer = traffic_analyzer

    def analyze_traffic(self):
        self.traffic_analyzer.analyze_traffic()
        print("Additional functionality added by TrafficAnalyzerDecorator.")


class TransportDecorator(Transport):
    def __init__(self, transport):
        self.transport = transport

    def move(self):
        self.transport.move()
        print("Additional functionality added by TransportDecorator.")

if __name__ == '__main__':
    traffic_analyzer = TrafficAnalyzer()
    transport_factory = TransportFactory()
    transport = transport_factory.create_transport('bus')
    transport_1 = transport_factory.create_transport('car')

    # Декорируем объекты
    traffic_analyzer = TrafficAnalyzerDecorator(traffic_analyzer)
    transport = TransportDecorator(transport)

    # Используем декорированные объекты
    traffic_analyzer.analyze_traffic()
    transport.move()